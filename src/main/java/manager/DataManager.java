package manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import model.Customer;
import model.Flight;
import model.Reservation;

public class DataManager {

	// private static final String PERSISTENCE_UNIT_NAME = "Eclipselink_JPA";
	// private static EntityManagerFactory factory =
	// Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
	// List<?> objects = new ArrayList<Object>();

	public static void addFlight(Flight flight) {
		/*
		 * EntityManager em = factory.createEntityManager();
		 * 
		 * em.getTransaction().begin(); em.persist(flight);
		 * em.getTransaction().commit();
		 * 
		 * em.close();
		 */
	}

	public static void addCustomer(Customer customer) {
		/*
		 * EntityManager em = factory.createEntityManager();
		 * 
		 * em.getTransaction().begin(); em.persist(customer);
		 * em.getTransaction().commit();
		 * 
		 * em.close();
		 * 
		 */

	}

	public static void addReservation(Reservation reservation) {
		/*
		 * EntityManager em = factory.createEntityManager();
		 * 
		 * em.getTransaction().begin(); em.persist(reservation);
		 * em.getTransaction().commit();
		 * 
		 * em.close();
		 */

	}

	// @SuppressWarnings("unchecked")
	public static List<Reservation> getUsersReservations(Long userId) {

		List<Reservation> reservations = new ArrayList<Reservation>();
		/*
		 * EntityManager em = factory.createEntityManager(); Query q =
		 * em.createQuery("select r from Reservation r"); List<Reservation>
		 * reservations = new ArrayList<Reservation>(); List<Reservation> list =
		 * q.getResultList();
		 * 
		 * for (Reservation reservation : list) { if
		 * (reservation.getIdCustomer().equals(userId))
		 * reservations.add(reservation); }
		 * 
		 * return reservations;
		 */

		return reservations;

	}

	public static void updateFlightCapacity(Long id) throws Exception {
		/*
		 * 
		 * Connection conn = null;
		 * 
		 * String userName = "a1263502"; String password = "kay2devi"; String
		 * url = "jdbc:mysql://a1263502.mysql.univie.ac.at/a1263502";
		 * 
		 * Class.forName("com.mysql.jdbc.Driver").newInstance(); conn =
		 * DriverManager.getConnection(url, userName, password);
		 * 
		 * System.out.println("Database connection established"); Statement st =
		 * conn.createStatement(); String q = "UPDATE FLIGHT " +
		 * "SET SEATSAVAILABLE = SEATSAVAILABLE - 1 " + "WHERE ID =" + id;
		 * 
		 * st.executeUpdate(q);
		 */

	}

	// @SuppressWarnings("unchecked")
	public static boolean usernameExists(String username) {
		/*
		 * EntityManager em = factory.createEntityManager(); Query q =
		 * em.createQuery("select c from Customer c"); List<Customer> list =
		 * q.getResultList();
		 * 
		 * for (Customer customer : list) { if (customer.getUsername() ==
		 * username) return true; }
		 * 
		 * return false;
		 */
		return true;
	}

	// @SuppressWarnings("unchecked")
	public static boolean userValid(String username, String password) {
		/*
		 * EntityManager em = factory.createEntityManager(); Query q =
		 * em.createQuery("select c from Customer c"); List<Customer> list =
		 * q.getResultList();
		 * 
		 * for (Customer customer : list) { if
		 * (customer.getUsername().equals(username) &&
		 * customer.getPassword().equals(password)) return true; }
		 * 
		 * return false;
		 */
		return true;
	}

	// @SuppressWarnings("unchecked")
	public static Customer getCustomer(String username, String password) {
		/*
		 * EntityManager em = factory.createEntityManager(); Query q =
		 * em.createQuery("select c from Customer c"); List<Customer> list =
		 * q.getResultList();
		 * 
		 * for (Customer customer : list) { if
		 * (customer.getUsername().equals(username) &&
		 * customer.getPassword().equals(password)) { return customer; } }
		 */

		return null;
	}

	// @SuppressWarnings("unchecked")
	public static boolean flightExists(Long id) {
		/*
		 * EntityManager em = factory.createEntityManager(); Query q =
		 * em.createQuery("select f from Flight f"); List<Flight> list =
		 * q.getResultList();
		 * 
		 * for (Flight flight : list) { if (flight.getId() == id) return true; }
		 * 
		 * return false;
		 */
		return true;
	}

	// @SuppressWarnings("unchecked")
	public static List<Flight> getFlightList() {
		/*
		 * EntityManager em = factory.createEntityManager(); Query q =
		 * em.createQuery("select f from Flight f"); List<Flight> list =
		 * q.getResultList();
		 * 
		 * return list;
		 */
		return null;

	}

	public static List<Document> getFlightList(String airline, String source, String destination, String date) {

		try {

			MongoClientURI uri = new MongoClientURI(
					"mongodb://imse_team3:ourcoolpassword@airlinebooking-shard-00-00-lxyuw.mongodb.net:27017,airlinebooking-shard-00-01-lxyuw.mongodb.net:27017,airlinebooking-shard-00-02-lxyuw.mongodb.net:27017/<DATABASE>?ssl=true&replicaSet=AirlineBooking-shard-0&authSource=admin");
			MongoClient mongoClient = new MongoClient(uri);
			MongoDatabase database = mongoClient.getDatabase("test");
			MongoCollection<Document> flights = database.getCollection("flights");

			BasicDBObject query = new BasicDBObject();

			if ((airline != null) && (airline != ""))
				query.append("airline", airline);
			if ((source != null) && (source != ""))
				query.append("source", source);
			if ((destination != null) && (destination != ""))
				query.append("destination", destination);
			if ((date != null) && (date != ""))
				query.append("date", date);

			List<Document> searchRes = flights.find(query).into(new ArrayList<Document>());

			return searchRes;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean addFlightSuccess(Flight flight) {
		/*
		 * if(!DataManager.flightExists(flight.getId())) {
		 * DataManager.addFlight(flight); return true; }
		 */

		MongoClientURI uri = new MongoClientURI(
				"mongodb://imse_team3:ourcoolpassword@airlinebooking-shard-00-00-lxyuw.mongodb.net:27017,airlinebooking-shard-00-01-lxyuw.mongodb.net:27017,airlinebooking-shard-00-02-lxyuw.mongodb.net:27017/<DATABASE>?ssl=true&replicaSet=AirlineBooking-shard-0&authSource=admin");
		MongoClient mongoClient = new MongoClient(uri);
		MongoDatabase database = mongoClient.getDatabase("test");
		MongoCollection<Document> flights = database.getCollection("flights");

		/*
		 * airline source destination date muaj/date/vit time ore/Min/sek price
		 * capacity
		 */

		Document addflight = new Document("airline", flight.getAirline()).append("source", flight.getSource())
				.append("destination", flight.getDestination()).append("date", flight.getDate())
				.append("time", flight.getTime()).append("price", flight.getPrice())
				.append("capacity", flight.getCapacity());

		flights.insertOne(addflight);
		return true;
	}

}
