package manager;
import model.Admin;
import model.Customer;

public class UserManager {
	

		public static boolean registerSuccess(Customer newCustomer){
			if(!DataManager.usernameExists(newCustomer.getUsername())) {
				DataManager.addCustomer(newCustomer);
				return true;
			}
			return false;
		}
		
		public static boolean loginValid(String username, String password) {
			if(DataManager.userValid(username, password)) return true;
			return false;
		}
		
		public static boolean isAdmin(String username, String password) {
			if(username.equals(Admin.getUsername()) && password.equals(Admin.getPassword())) return true;
			return false;
		}
}
