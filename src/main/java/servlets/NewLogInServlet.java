package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import manager.UserManager;
import model.Admin;
import model.Customer;


/**
 * Servlet implementation class NewLogInServlet
 */
@WebServlet("/NewLoginServlet")
public class NewLogInServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewLogInServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		if (UserManager.isAdmin(username, password)) {
			Admin admin = new Admin();
			HttpSession session = request.getSession(true);
			session.setAttribute("currentSessionCustomer", admin);
			response.sendRedirect("loginSuccessfulAdmin.jsp");
			
		}
		else {
			if (UserManager.loginValid(username, password)) {
		//	Customer customer = DataManager.getCustomer(username, password);
			HttpSession session = request.getSession(true);
		//	session.setAttribute("currentSessionCustomer", customer);
			response.sendRedirect("loginSuccessful.jsp");
		}	
		else
			response.sendRedirect("loginInvalid.jsp");
		}
	
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
