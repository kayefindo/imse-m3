package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import manager.DataManager;
import model.Admin;
import model.Customer;
import model.Flight;
import model.Reservation;

/**
 * Servlet implementation class BookingServlet
 */
public class BookingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookingServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String page = "";

		HttpSession session = request.getSession(true);

		if (session.getAttribute("currentSessionCustomer") instanceof Admin) {
			response.sendRedirect("error.jsp"); // the Admin cannot perform
												// booking, something went
												// wrong!
		}

		else {
			Customer currentC = (Customer) session.getAttribute("currentSessionCustomer");

			Long idCustomer = currentC.getId();

			String[] flightIds = request.getParameterValues("checkedFlights");
			
			if (flightIds==null)
				response.sendRedirect("SearchServlet"); //if none flights checked back to search
			else {

				List<Flight> checkedFlights = getCheckedFlights(flightIds);

				for (Flight f : checkedFlights) {
					Reservation newReservation = new Reservation();
					newReservation.setIdCustomer(idCustomer);
					newReservation.setIdFlight(f.getId());
					newReservation.setPrice(f.getPrice());
					try {
						DataManager.updateFlightCapacity(f.getId());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						page = "error.jsp";
					}
					DataManager.addReservation(newReservation);
				}

				page = "bookingSuccessful.jsp";
			
				RequestDispatcher dd=request.getRequestDispatcher(page);
				dd.forward(request, response);

			}
		}

	}

	protected List<Flight> getCheckedFlights(String[] ids) {
		List<Flight> checkedVs = new ArrayList<Flight>();
		List<Flight> listofall = DataManager.getFlightList();

		for (int i = 0; i < ids.length; ++i) {
			for (Flight f : listofall) {
				if (Long.parseLong(ids[i]) == f.getId())
					checkedVs.add(f);
			}
		}

		System.out.println("The size of the new list is: " + checkedVs.size()); // only
																				// for
																				// testing
																				// purposes

		return checkedVs;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
