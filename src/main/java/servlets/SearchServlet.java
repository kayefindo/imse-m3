package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.Document;

import manager.DataManager;
import model.Flight;

/**
 * Servlet implementation class SearchServlet
 */

@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Document> list;
		if (request.getParameter("reset")!=null){
			request.setAttribute("airline", "");
			request.setAttribute("source", "");
			request.setAttribute("dest", "");
			request.setAttribute("date", "");
			list = DataManager.getFlightList("","","","");
		}
		else {
			request.setAttribute("airline", request.getParameter("airline"));
			request.setAttribute("source", request.getParameter("source"));
			request.setAttribute("dest", request.getParameter("dest"));
			request.setAttribute("date", request.getParameter("date"));
			list = DataManager.getFlightList(request.getParameter("airline"), request.getParameter("source"),
					request.getParameter("dest"), request.getParameter("date"));
		}
		
		request.setAttribute("list", list);
		request.getRequestDispatcher("flights.jsp").forward(request, response);
		
	}
		

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
