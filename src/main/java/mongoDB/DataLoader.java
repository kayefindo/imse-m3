package mongoDB;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import model.Customer;
import model.Flight;
import model.Reservation;



public class DataLoader {
	private static final String PERSISTENCE_UNIT_NAME = "Eclipselink_JPA";
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
	List<?> objects = new ArrayList<Object>();

	@SuppressWarnings("unchecked")
	public static List<Reservation> getReservations() {
		EntityManager em = factory.createEntityManager();
		Query q = em.createQuery("select r from Reservation r");
		return q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public static List<Customer> getCustomer() {
		EntityManager em = factory.createEntityManager();
		Query q = em.createQuery("select c from Customer c");
		return q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public static boolean flightExists(Long id) {
		EntityManager em = factory.createEntityManager();
		Query q = em.createQuery("select f from Flight f");
		List<Flight> list = q.getResultList();

		for (Flight flight : list) {
			if (flight.getId() == id)
				return true;
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	public static List<Flight> getFlightList() {
		EntityManager em = factory.createEntityManager();
		Query q = em.createQuery("select f from Flight f");
		return q.getResultList();
	}

	public static List<Flight> getFlightList(String airline, String source, String destination, String date) {

		EntityManager em = factory.createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Flight> q = cb.createQuery(Flight.class);
		Root<Flight> root = q.from(Flight.class);

		List<Predicate> predicates = new ArrayList<Predicate>();
		if ((airline != null) && (airline != ""))
			predicates.add(cb.equal(root.get("airline"), airline));
		if ((source != null) && (source != ""))
			predicates.add(cb.equal(root.get("source"), source));
		if ((destination != null) && (destination != ""))
			predicates.add(cb.equal(root.get("destination"), destination));
		if ((date != null) && (date != ""))
			predicates.add(cb.equal(root.get("date"), date));

		q.select(root).where(predicates.toArray(new Predicate[] {}));

		return (List<Flight>) em.createQuery(q).getResultList();
	}
	

}
