package mongoDB;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import model.Customer;
import model.Flight;
import model.Reservation;
import mongodbModel.MCustomer;
import mongodbModel.MReservation;


public class DataMigrator {
	static List<Customer> oldCustomers;
	static List<Flight> oldFlights;
	static List<Reservation> reservations;
	static List<MCustomer> mCustomers;
	static MongoClient mongoClient;
	static MongoDatabase database;
	static MongoCollection<Document> flights;
	static MongoCollection<Document> customers;
	
	public static void main(String[] args){
		
		oldFlights = DataLoader.getFlightList();
		oldCustomers = DataLoader.getCustomer();
		reservations = DataLoader.getReservations();
		mCustomers=new ArrayList<MCustomer>();
		
		
		try{

			MongoClientURI  uri = 
	        		 new MongoClientURI("mongodb://imse_team3:ourcoolpassword@airlinebooking-shard-00-00-lxyuw.mongodb.net:27017,airlinebooking-shard-00-01-lxyuw.mongodb.net:27017,airlinebooking-shard-00-02-lxyuw.mongodb.net:27017/<DATABASE>?ssl=true&replicaSet=AirlineBooking-shard-0&authSource=admin"); 
			mongoClient = new MongoClient(uri);
	        database = mongoClient.getDatabase("test");
	        flights = database.getCollection("flights");
	        flights.drop();
	        customers = database.getCollection("customers");
	        customers.drop();
	        pumpData();
	        
	        
	        /*
	         * some example on getting required stuff
	         * 
	         */
	        MongoCursor<Document> cursor  = customers.find().iterator();
	        while (cursor.hasNext()){
	        	Document fl = cursor.next();
	        	List<String> reserves = (List<String>) fl.get("reservations");
	        	String name = (String) fl.get("username");
	        }
	        System.out.println("Success");
	        
	      
	      }catch(Exception e){
	         e.printStackTrace();
	      }
		
		
	}
	
	private static void pumpData(){
		
		List<MReservation> mReservations = new ArrayList<MReservation>();
		
		//pushing flights; updating flights ids in the reservations
		for (Flight f : oldFlights){
			Document flight = new Document("airline",f.getAirline())
								.append("source", f.getSource())
								.append("destination", f.getDestination())
								.append("date", f.getDate())
								.append("time", f.getTime())
								.append("price", f.getPrice())
								.append("capacity", f.getCapacity())
								.append("seatsAvailable", f.getSeatsAvailable());
			flights.insertOne(flight);
			long old = f.getId();
			String newID = flight.get("_id").toString();
			for (Reservation r : reservations)
				if (old==r.getIdFlight())
					mReservations.add(new MReservation(Long.toString(r.getIdCustomer()), newID));
		}
		
		
		//pushing customers with updated reservations
		for (Customer c : oldCustomers){
			List<String> customerReservations = new ArrayList<String>();
			for (MReservation m : mReservations)
				if (c.getId()==Long.parseLong(m.getCustomer()))
					customerReservations.add(m.getFlight());
			
			Document customer = new Document("username",c.getUsername())
									.append("password", c.getPassword())
									.append("firstname", c.getFirstName())
									.append("lastname", c.getLastName())
									.append("email", c.getEmail())
									.append("telnr", c.getTelNr())
									.append("reservations", customerReservations);
			customers.insertOne(customer);
		}
		
		 
	}

}
