package mongodbModel;

import java.util.ArrayList;
import java.util.List;

import model.Customer;

public class MCustomer {
	private Long id;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private int telNr;
	private List<String> reservations;

	public MCustomer() {
		super();
		reservations = new ArrayList<String>();
	}
	
	public MCustomer(Customer customer) {
		id=customer.getId();
		username=customer.getUsername();
		password=customer.getPassword();
		firstName=customer.getLastName();
		lastName=customer.getLastName();
		email=customer.getEmail();
		telNr=customer.getTelNr();
		reservations = new ArrayList<String>();
	}

	public String getFirstName() {
		return firstName;
	}

	public void addReservation(String flightId) {
		reservations.add(flightId);
	}
	
	public List<String> getReservations() {
		return reservations;
	}

	public void setReservations(List<String> reservations) {
		this.reservations = reservations;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getTelNr() {
		return telNr;
	}

	public void setTelNr(int telNr) {
		this.telNr = telNr;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


}
