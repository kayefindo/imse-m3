package mongodbModel;

public class MReservation {
	public String customer;
	public String flight;
	
	public MReservation(String customer, String flight) {
		super();
		this.customer = customer;
		this.flight = flight;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getFlight() {
		return flight;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}

}
