<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Register</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<!-- Bootstrap Core CSS -->
<link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="Bootstrap/css/stylish-portfolio.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="Bootstrap/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic"
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body>
	<header id="top" class="header">
	<div class="text-vertical-center">
		<h3>Registration Form:</h3>
		<form action="RegisterServlet">
			Username:<br> <font color="black"> <input type="text"
				style="text-align: center" name="username" />
			</font><br /> <br /> 
			
			Password:<br> <font color="black"> <input
				type="password" style="text-align: center" name="password" />
			</font><br /> <br /> 
			
			First name:<br> <font color="black"> <input
				type="text" style="text-align: center" name="firstname" />
			</font> <br /> <br /> 
			
			Last name:<br> <font color="black"> <input
				type="text" style="text-align: center" name="lastname" />
			</font> <br /> <br /> 
			
			E-Mail:<br> <font color="black"> <input
				type="text" style="text-align: center" name="email" />
			</font> <br /> <br /> 
			
			Phone Nr:<br> <font color="black"> <input
				type="text" style="text-align: center" name="tel" />
			</font> <br /> <br /> <input class="btn btn-dark btn-lg" type="submit"
				value="Register" />
		</form>
		
	</div>
	</header>
</body>
</html>