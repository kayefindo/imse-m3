<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Invalid Login</title>

<link href="Bootstrap/css/popup.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
<!--
	function toggle_visibility(id) {
		var e = document.getElementById(id);
		if (e.style.display == 'block')
			e.style.display = 'none';
		else
			e.style.display = 'block';
	}
//-->
</script>

</head>
<body>

	<!-- Popupbox -->

	<div id="popup-box1" class="popup-position">
		<div id="popup-wrapper">
			<div id="popup-container">
				<h3 align="center">Invalid Login</h3>
				<p align="center">Sorry, your username or password is incorrect!</p>
				<p align="center">Try again.</p>
				<p align="center">
					<a href="login.jsp">Back to Login</a>
				</p>
			</div>
		</div>
	</div>

</body>
</html>