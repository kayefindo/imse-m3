<%@page import="manager.DataManager"%>
<%@page import="model.Reservation"%>
<%@page import="model.Customer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Bootstrap Core CSS -->
<link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="Bootstrap/css/stylish-portfolio.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="Bootstrap/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic"
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


<style>
body {
	background-color: white;
}

table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	text-align: center;
	padding: 8px;
}

tr {
	background-color: rgba(0, 0, 0, 0.7);
	text-align: center;
	color: white;
}

th {
	background-color: rgba(0, 0, 0, 0.7);
	color: white;
}
</style>

<title>My Reservations</title>
</head>
<body>
	<%
		HttpSession sess = request.getSession();
		Customer currentUser = (Customer) sess.getAttribute("currentSessionCustomer");
		String name = currentUser.getFirstName();
		name = name + "'s";

		List<Reservation> list = new ArrayList<Reservation>();
		list = DataManager.getUsersReservations(currentUser.getId());
		
	%>
	<header id="top" class="header">
	<div class="text-vertical-center">
		<h3><%=name%> Reservations:</h3>
	
		<table>
			<tr>
				<th>Reservation ID</th>
				<th>Flight ID</th>
				<th>Price</th>
				
			</tr>
			<%
				for (Reservation r : list) {
					
								%>
			<tr>
				<td><%=r.getId()%></td>
				<td><%=r.getIdFlight()%></td>
				<td><%=r.getPrice()%></td>
			
			</tr>
			<%
				}
			%>
		</table>
		
		<br> <button type="button" name="back" onclick="history.back()">Back</button>
	</div>
	</header>


</body>
</html>