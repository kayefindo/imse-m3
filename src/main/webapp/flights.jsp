<%@page import="org.bson.Document"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Bootstrap Core CSS -->
<link href="Bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="Bootstrap/css/stylish-portfolio.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="Bootstrap/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic"
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


<style>
body {
	background-color: white;
}

table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	text-align: center;
	padding: 8px;
}

tr {
	background-color: rgba(0, 0, 0, 0.7);
	text-align: center;
	color: white;
}

th {
	background-color: rgba(0, 0, 0, 0.7);
	color: white;
}

</style>

<title>Flight List</title>
</head>
<body>

	<%
		String airline = "";
		String source = "";
		String dest = "";
		String date = "";
		List<Document> list = new ArrayList();
		if (request.getAttribute("list")!=null)
			list = (List<Document>)request.getAttribute("list");
		if (request.getAttribute("airline")!=null)
			airline = (String) request.getAttribute("airline");
		if (request.getAttribute("source")!=null)
			source = (String) request.getAttribute("source");
		if (request.getAttribute("dest")!=null)
		 	dest = (String) request.getAttribute("dest");
		if (request.getAttribute("date")!=null)
		 	date = (String) request.getAttribute("date");
	%>
	
	<header id="top" class="header">
	<div class="text-vertical-center">
		<h3>Flights</h3>
		
		<form action="SearchServlet">
		
		<div align="center">
				Search by Airline:<br> <font color="black"> <input type="text"
					name="airline" value ="<%=airline%>" />
				</font>
				<br />
		</div>
		
		<div align="center">
				Search by Source:<br> <font color="black"> <input
					type="text" name="source" value ="<%=source%>"/>
				</font>
				</div>
		<div align="center">
				Search by Destination:<br> <font color="black"> <input
					type="text" name="dest" value ="<%=dest%>"/>
				</font>
				</div>
		<div align="center">
				Search by Date:<br> <font color="black"> <input
					type="text" name="date" value ="<%=date%>" />
				</font>
				</div>
				
				<br /> 
				<input class="btn btn-dark btn-lg" type="submit" value="Search" />
				<input class="btn btn-dark btn-lg" type="submit" name = "reset" value="Reset" />
			<br> <br>

		</form>
			<h3>List of all flights: </h3>
		
		<form action="BookingServlet" >
			<table>
				<tr>
					<th>ID</th>
					<th>Source</th>
					<th>Destination</th>
					<th>Date</th>
					<th>Time</th>
					<th>Price</th>
					<th>Airline</th>
					<th>Free Seats</th>
					<th>Check flight(s): </th>
				</tr>
			<%
					if (list.isEmpty()) {
			%>
				</table>
			<h2>No flights found</h2>
			<% } else {
						
					for (Document f : list) {
						double price = (Double) f.get("price");
						
				%>

				<tr>
					<td><%=f.get("_id")%></td>
					<td><%=f.get("source")%></td>
					<td><%=f.get("destination")%></td>
					<td><%=f.get("date")%></td>
					<td><%=f.get("time")%></td>
					<td><%=String.valueOf(price)%></td>
					<td><%=f.get("airline")%></td>
					<td><%=f.get("seatsAvailable")%></td>
					
					
					<td><input type="checkbox" name="checkedFlights" value="<%=f.get("_id")%>"></td>
				
					<td><a href="#bottom" class="btn btn-dark btn-lg">Done</a></td>
					
					
					</tr>
				<% } %>
					</table>
			<% } %>
					
				
			

			<br> <input class="btn btn-dark btn-lg" type="submit"
				value="Book selected" />
			
			<a id="bottom"></a>
		</form>
		<br> <a href="loginSuccessful.jsp" class="btn btn-dark btn-lg">Back</a>
		<br>
	</div>
	</header>


</body>
</html>