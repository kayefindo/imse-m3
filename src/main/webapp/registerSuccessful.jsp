<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register Success</title>

<link href="Bootstrap/css/popup.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
<!--
	function toggle_visibility(id) {
		var e = document.getElementById(id);
		if (e.style.display == 'block')
			e.style.display = 'none';
		else
			e.style.display = 'block';
	}
//-->
</script>

</head>
<body>

	<!-- Popupbox -->

	<div id="popup-box1" class="popup-position">
		<div id="popup-wrapper">
			<div id="popup-container">
				<h3 align="center">Register Success</h3>
				<p align="center">Your registration was successful!</p>
				<p align="center">Now you can login:</p>
				<p align="center">
					<a href="login.jsp">Login now</a><br>
				<p align="center">Or go back to our homepage:</p>
				<p align="center">
					<a href="homepage.jsp">Home</a>
				</p>
				<br>
			</div>
		</div>
	</div>

</body>
</html>